import subprocess

from expects import expect, equal
from mamba import description, it
import main

CORRECT_OUTPUT = "b'1\\n2\\nfizz\\n4\\n5\\nfizz\\n7\\n8\\nfizz\\n10\\n11\\nfizz\\n13\\n14\\nfizz\\n'"

with description(main) as self:
    with it('test main'):
        output = subprocess.check_output(['python', 'main.py', '15'])
        expect(str(output)).to(equal(CORRECT_OUTPUT))
