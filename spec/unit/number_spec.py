from expects import expect, equal
from mamba import description, it

from src.number import Number

with description(Number) as self:
    with it('create correctly'):
        number = Number(5)
        expect(5).to(equal(number.value))

    with description('.is_divisible_for'):
        with it('returns true if is divisible'):
            number = Number(5)
            divisible = number.is_divisible_for(Number(5))
            expect(divisible).to(equal(True))

        with it('returns false if is not divisible'):
            number = Number(5)
            divisible = number.is_divisible_for(Number(2))
            expect(divisible).to(equal(False))
