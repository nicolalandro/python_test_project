from expects import expect, equal
from mamba import description, before, it

from src.fizz_buzzer import FizzBuzzer
from src.number import Number

with description(FizzBuzzer) as self:
    with before.all:
        self.fizz_buzzer = FizzBuzzer()

    with it('say number'):
        number = Number(2)
        speach_text = self.fizz_buzzer.speach_text_for(number)
        expect(speach_text).to(equal('2'))

    with it('say fizz'):
        number = Number(3)
        speach_text = self.fizz_buzzer.speach_text_for(number)
        expect(speach_text).to(equal('fizz'))
