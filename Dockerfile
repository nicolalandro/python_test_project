FROM python:3.5-alpine
WORKDIR /app
ADD requirements.txt requirements.txt
RUN pip install -r requirements.txt
ADD ./ ./
CMD mamba --enable-coverage && coverage report -m
